package annotation.test;

import annotation.ArticleCalculator;
import annotation.ArticleManager;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.mockito.Mockito.when;

/**
 * inject mock with spy
 */
public class ArticleManagerTest {

    // spy 默认会调用真实方法
    @Spy
    private ArticleCalculator articleCalculator;
    @InjectMocks
    private ArticleManager articleManager;

    @Test
    public void injectTest() {
        MockitoAnnotations.initMocks(this);
        articleCalculator.setName("zhao");
        Assert.assertEquals("zhao", articleManager.getArticleName());
    }
}
