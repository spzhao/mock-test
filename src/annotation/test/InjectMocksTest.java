package annotation.test;

import annotation.ArticleCalculator;
import annotation.ArticleManager;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.when;

/**
 * inject mock test
 */
public class InjectMocksTest {

    @Mock
    private ArticleCalculator articleCalculator;
    @InjectMocks
    private ArticleManager articleManager;

    @Test
    public void injectMocksTest() {
        MockitoAnnotations.initMocks(this);
        when(articleCalculator.getName()).thenReturn("zhao","zhao2");

        Assert.assertEquals("zhao",articleCalculator.getName());
        Assert.assertEquals("zhao2", articleManager.getArticleName());
    }
}
