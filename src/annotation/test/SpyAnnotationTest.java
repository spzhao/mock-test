package annotation.test;

import annotation.ArticleCalculator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

/**
 * 21 spy annotation
 */
public class SpyAnnotationTest {
    @Spy
    ArticleCalculator spyA;

    @Test
    public void captorTest() {
        MockitoAnnotations.initMocks(this);
        spyA.setName("li");
        Assert.assertEquals("li", spyA.getName());
    }
}
