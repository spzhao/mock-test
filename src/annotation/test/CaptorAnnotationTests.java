package annotation.test;

import annotation.ArticleCalculator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * 21 captor annotation
 */
public class CaptorAnnotationTests {

    @Captor
    ArgumentCaptor<String> argumentCaptor;

    @Test
    public void captorTest() {
        // 必须
        MockitoAnnotations.initMocks(this);
        ArticleCalculator articleCalculator = mock(ArticleCalculator.class);
        articleCalculator.setName("zhao");
        verify(articleCalculator).setName(argumentCaptor.capture());
        Assert.assertEquals("zhao", argumentCaptor.getValue());
    }
}
