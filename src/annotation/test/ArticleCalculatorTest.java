package annotation.test;

import annotation.ArticleCalculator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * <p>9. Shorthand for mocks creation - @Mock annotation</p>
 * <p>10. Stubbing consecutive calls (iterator-style stubbing)</p>
 */
public class ArticleCalculatorTest {
    // 注解引入，相当于 calculator = mock(ArticleCalculator.class)
    @Mock
    private ArticleCalculator calculator;

    @Before
    public void setUp() throws Exception {
        // 必须
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNameTest() {
        when(calculator.getName()).thenReturn("zhao", "shengping");
        Assert.assertEquals("zhao", calculator.getName());
        // 调用一次
        verify(calculator).getName();
        Assert.assertEquals("shengping", calculator.getName());
        // 调用了2次
        verify(calculator, times(2)).getName();
        // 没有需要verify的mock了
        verifyNoMoreInteractions(calculator);
    }
}