package annotation;

public class ArticleCalculator {
    private String name;

    public ArticleCalculator() {
    }

    public ArticleCalculator(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("set Name : " + name);
        this.name = name;
    }
}
