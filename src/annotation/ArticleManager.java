package annotation;

public class ArticleManager {

    private ArticleCalculator articleCalculator;

    public ArticleManager(ArticleCalculator articleCalculator) {
        this.articleCalculator = articleCalculator;
    }

    public String getArticleName() {
        return articleCalculator.getName();
    }
}
