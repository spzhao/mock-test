package base;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatcher;

import javax.crypto.Mac;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MatcherTest {
    @Test
    public void matcherTest() {
        List mockedList = mock(List.class);
        when(mockedList.get(anyInt())).thenReturn("element");
        when(mockedList.contains(argThat(isValid()))).thenReturn(true);

        System.out.println(mockedList.get(999));
        // Assert.assertEquals(mockedList.get(999), "element");

        verify(mockedList).get(anyInt());

        // mockedList.add("hello");  // pass
        mockedList.add("hello1");
        verify(mockedList).add(argThat(new MyMatcher()));
    }

    private Matcher<String> isValid() {
        return new MyMatcher();
    }

    /**
     * 自定义matcher
     */
    class MyMatcher extends ArgumentMatcher<String> {

        @Override
        public boolean matches(Object argument) {
            String _item = (String) argument;
            return "hello".equals(_item);
        }
    }
}
