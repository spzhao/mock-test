package base;


import org.junit.Assert;
import org.junit.Test;
import org.junit.internal.runners.statements.ExpectException;

import java.util.LinkedList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StubTest {

    @Test(expected = RuntimeException.class)
    public void stubTest() {
        LinkedList mockedLinkedList = mock(LinkedList.class);
        when(mockedLinkedList.get(0)).thenReturn("first");
        when(mockedLinkedList.get(1)).thenThrow(new RuntimeException());
        System.out.println(mockedLinkedList.get(0));
        Assert.assertEquals("first", mockedLinkedList.get(0));

        // RuntimeException
        System.out.println(mockedLinkedList.get(1));
    }
}
