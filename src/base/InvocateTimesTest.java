package base;

import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.*;

public class InvocateTimesTest {

    @Test
    public void invokeOnceTest() {
        List mockList = mock(List.class);
        mockList.add("once");
        // 一次
        verify(mockList).add("once");
    }

    @Test
    public void invokeTwiceTest() {
        List mockList = mock(List.class);
        mockList.add("twice");
        mockList.add("twice");
        // 2次
        verify(mockList, times(2)).add("twice");
    }

    @Test
    public void invokeThreeTest() {
        List mockList = mock(List.class);
        mockList.add("three times");
        mockList.add("three times");
        mockList.add("three times");
        // 3次
        verify(mockList, times(3)).add("three times");
    }

    @Test
    public void invokeNeverTest() {
        List mockList = mock(List.class);
        mockList.add("three times");
        mockList.add("three times");
        mockList.add("three times");
        // 3次
        verify(mockList, never()).add("three times 1");
    }


    @Test
    public void invokeAtLeastTest() {
        List mockList = mock(List.class);
        mockList.add("three times");
        mockList.add("three times");
        mockList.add("three times");
        // 3次
        verify(mockList, atLeastOnce()).add("three times");
        verify(mockList, atLeast(2)).add("three times");
        verify(mockList, atMost(10)).add("three times");
    }

    // never happened test
    // 从未调用某个mock类
    @Test
    public void neverHappenedTest() {
        List mockList = mock(List.class);
        List mockList2 = mock(List.class);
        mockList.add("three times");
        verifyZeroInteractions(mockList2);
    }

    // finding redundant invocations
    // redundant：冗余
    @Test
    public void redundantInvocationsTest() {
        List mockList = mock(List.class);
        mockList.add("one");
        mockList.add("two");
        verify(mockList).add("one");
        // fail
        // verifyNoMoreInteractions(mockList);

        verify(mockList).add("two");
        // success
        // 所有mock的方法调用都被verify了
        verifyNoMoreInteractions(mockList);
    }
}
