package base;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

/**
 * 17 Resetting mocks
 */
public class ResetMockTest {

    @Test
    public void resetMockTest() {
        List mockList = mock(List.class);
        when(mockList.size()).thenReturn(10);
        mockList.add(1);
        Assert.assertEquals(10, mockList.size());
        reset(mockList);
        // 重置后原来的stub将失效
        Assert.assertEquals(0, mockList.size());
    }
}
