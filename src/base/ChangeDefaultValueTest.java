package base;

import annotation.ArticleCalculator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;

import static org.mockito.Mockito.mock;

/**
 * 14 default return values of unstubbed invocations
 */
public class ChangeDefaultValueTest {
    @Test
    public void changeDefaultMockValueTest() {
        Answer answer = invocation -> {
            String name = invocation.getMethod().getName();
            Object[] arguments = invocation.getArguments();
            System.out.println("method name=" + name);
            System.out.println("method arguments =" + Arrays.toString(arguments));
            if ("getName".equals(name)) {
                return "zhao";
            }
            return null;
        };
        ArticleCalculator articleCalculator = mock(ArticleCalculator.class, answer);
        articleCalculator.setName("li");
        Assert.assertEquals("zhao", articleCalculator.getName());
    }
}
