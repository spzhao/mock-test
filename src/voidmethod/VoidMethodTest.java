package voidmethod;

import annotation.ArticleCalculator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * 12 doReturn() | doThrow | doAnswer | doNothing | doCallRealMethod
 */
public class VoidMethodTest {

    // void 抛出异常
    @Test(expected = RuntimeException.class)
    public void voidMethodShouldThrowErrorTest() {
        ArticleCalculator articleCalculator = mock(ArticleCalculator.class);
        doThrow(new RuntimeException()).when(articleCalculator).setName(anyString());
        articleCalculator.setName("zhao");
    }

    // do answer
    @Test
    public void voidDoAnswerMethodTest() {
        ArticleCalculator articleCalculator = mock(ArticleCalculator.class);
        final String[] cacheName = {"zhao_cache"};
        doAnswer(invocation -> {
            // do something here
            Object[] arguments = invocation.getArguments();
            System.out.println("getArguments len = " + arguments.length);
            if (arguments.length > 0) {
                String arg1 = (String) arguments[0];
                System.out.println("arg1 = " + arg1);
                cacheName[0] = arg1;
            }
            return null;
        }).when(articleCalculator).setName(anyString());
        articleCalculator.setName("zhao");
        Assert.assertEquals(cacheName[0], "zhao");
        Assert.assertNull(articleCalculator.getName());
    }

    // doNothing | doCallRealMethod
    @Test
    public void doNothingTest() {
        ArticleCalculator articleCalculator = mock(ArticleCalculator.class);
        // 如果不加这行，则不会输出 set Name : xxx
        doCallRealMethod().when(articleCalculator).setName(anyString());
        // 调用实际方法
        doCallRealMethod().when(articleCalculator).getName();
        articleCalculator.setName("zhao");
        doNothing().when(articleCalculator).setName(anyString());
        articleCalculator.setName("li");
        Assert.assertEquals("zhao", articleCalculator.getName());
    }

    // doReturn
    @Test
    public void doReturnTest() {
        ArticleCalculator articleCalculator = mock(ArticleCalculator.class);
        doReturn("zhao").when(articleCalculator).getName();
        Assert.assertEquals("zhao", articleCalculator.getName());
        Assert.assertEquals("zhao", articleCalculator.getName());
        Assert.assertEquals("zhao", articleCalculator.getName());
        Assert.assertEquals("zhao", articleCalculator.getName());

        // 对比 when().thenReturn， 效果是一样的, thenReturn 可以配置多个返回
        List mockList = mock(List.class);
        when(mockList.get(anyInt())).thenReturn("zhao", "zhao1", "zhao2");
        Assert.assertEquals("zhao", mockList.get(1));
        Assert.assertEquals("zhao1", mockList.get(1));
        Assert.assertEquals("zhao2", mockList.get(1));
    }
}
