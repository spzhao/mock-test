package spy;

import annotation.ArticleCalculator;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;


public class SpyTest {

    /**
     * 13 Spying on real objects
     */
    @Test
    public void baseSpyTest() {
        List list = new LinkedList();
        List spyList = spy(list);
        // stub的方法不会被调用真实方法
        when(spyList.size()).thenReturn(100);
        list.add(1);
        list.add(2);
        spyList.add(1);
        spyList.add(2);
        Assert.assertEquals(100, spyList.size());
        Assert.assertEquals(1, spyList.get(0));
        Assert.assertEquals(2, list.size());

        // Important gotcha on spying real objects!

        // IndexOutOfBoundsException because size is 2 now;
        // when(spyList.get(3)).thenReturn(3);

        // it is ok for using doReturn to stub
        doReturn(3).when(spyList).get(3);
        Assert.assertEquals(3, spyList.get(3));
    }

    /**
     * 16 Real partial mocks
     */
    @Test
    public void partialMocksTest() {
        ArticleCalculator articleCalculator = new ArticleCalculator("zhao");
        ArticleCalculator spyA = spy(articleCalculator);
        spyA.setName("li");
        Assert.assertEquals("li", spyA.getName());

        // by mock
        ArticleCalculator mockA = mock(ArticleCalculator.class);
        doCallRealMethod().when(mockA).setName(anyString());
        when(mockA.getName()).thenCallRealMethod();
        mockA.setName("li2");
        Assert.assertEquals("li2",mockA.getName());
    }
}
