package argument;

import annotation.ArticleCalculator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * 15 capturing arguments for further assertions
 */
public class ArgumentCaptureTest {
    @Test
    public void argumentCaptureTest() {
        ArticleCalculator articleCalculator = mock(ArticleCalculator.class);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        articleCalculator.setName("zhao");
        verify(articleCalculator).setName(argumentCaptor.capture());
        Assert.assertEquals("zhao", argumentCaptor.getValue());
    }
}
